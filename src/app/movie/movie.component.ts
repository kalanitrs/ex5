import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.css']
})

export class MovieComponent implements OnInit {
  @Input() data:any;
  @Output() deleteMe = new EventEmitter<any>();

  Id;
  Title;
  Studio;
  Weekend_Income;
  showMe = true;

  constructor() { }

  ngOnInit() {
    this.Id = this.data.Id;
    this.Title = this.data.Title;
    this.Studio = this.data.Studio;
    this.Weekend_Income = this.data.Weekend_Income;
  }

  deleteRow() {
    this.showMe = false;
    this.deleteMe.emit(this);
  }
}
